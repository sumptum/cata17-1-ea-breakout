

# Boilerplate Code

Creating a bare-minimum C file for setting up SDL and OpenGL  with a single window and renderer (result is **glboilerplate.c**)

## pongy.c

> Compile using `make pongy`

Original file provided by Eike Anderson as part of an exercise to introduce OpenGL/SDL

## pongy_modified.c

> Compile using `make pongymod`

Completed pongy.c file (collisions, paddle movement and scoring system)

## sdlboilerplate.c 

> Compile using `make sdlboilerplate`
