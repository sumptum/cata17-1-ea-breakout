
#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>

int main(void) {

  /*
   * RETURN CODES
   *  0: Success
   *  1: SDL init fail
   *  2: Error creating Window
   *  3: Error creating renderer
   *
   */
  
  /* ----------------
   *  SDL Init
   * ----------------
   */
  if (SDL_Init(SDL_INIT_EVERYTHING)!=0) {
    perror("[FAILED] Initialising SDL: ");
    return 1;
  }

  int go; //Var for loop control
  
  /* ----------------
   *  Main Window
   * ----------------
   * Create the main output Window for SDL
   */

  // Window vars
  int winPosX = 100; int winPosY = 100;
  int winWidth = 640; int winHeight = 480;

  Uint32 timer; // Animation timer (ms)

  SDL_Window *window1 = SDL_CreateWindow(
    "Main Window",
    winPosX, winPosY,
    winWidth, winHeight,
    SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL
  );
  if (window1==NULL) {
    fprintf(stderr, "[FAILED] Creating Window window1");
    return 2; //Error creating Window
  }

  /*
   * ----------------
   *  Renderer
   * ----------------
   * Creates the main renderer (tied to main window)
   */

  // SDL_Renderer * renderer1 = SDL_CreateRenderer(window1, -1, 0);
  // if (renderer1==NULL) {
  //   fprintf(stderr, "[FAILED] Creating renderer renderer1 for window1");
  //   return 3;
  // }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 1); // Specifies the version of the OpenGL context
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5); // (here, 1.5) -- hello, 2003!
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY); // Allows deprecated functions to be used
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16); // Set depth buffer to 16 bits, if OpenGL agrees (see below)
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // Enable double buffering

  SDL_GLContext context1 = SDL_GL_CreateContext(window1); // Use SDL_GL_MakeCurrent function to switch contexts
  
  if (context1==NULL) {
    fprintf(stderr, "[FAILED] Creating render context context1 for window1");
    return 3;
  }

  glFrontFace(GL_CCW); // Counter-clockwise winding
  glEnable(GL_NORMALIZE); // Vectors normalized after transforming to keep units consistent for lighting
  glShadeModel(GL_SMOOTH); // GL_FLAT for flat shading instead
  glEnable(GL_DEPTH_TEST); // Enables depth comparisons
  
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glMatrixMode(GL_PROJECTION); // Sets our matrix mode (initially) to work with the projection stack
  glLoadIdentity(); // Projection matrix is now the identity matrix, so we can work in 2D space
  
  gluOrtho2D(
    -1.0*(GLdouble)(winWidth/2),  // Left
    (GLdouble)(winWidth/2),       // Right
    -1.0*(GLdouble)(winHeight/2), // Bottom
    (GLdouble)(winHeight/2)       // Top
  );

  //gluOrthof(
  //  -1.0*(GLfloat)(winWidth/2),
  //  (GLfloat)(winWidth/2),
  //  -1.0*(GLfloat)(winHeight/2),
  //  (GLfloat)(winHeight/2),
  //  (GLfloat)(-1),
  //  (GLfloat)(1)
  //); // Set up 2D orthographic projection matrix, so now we're officially in 2D

  glViewport(0,0,winWidth,winHeight); // Set the viewport to fill the window

  timer=SDL_GetTicks(); // Initialise the timer

  go=1;
  while(go) {

    /*
     * ----------------
     *  Event handling
     * ----------------
     */
    SDL_Event incomingEvent;
    while (SDL_PollEvent(&incomingEvent)) {
      switch (incomingEvent.type) {
        case SDL_QUIT:
          // User requested program quit - e.g. window close
          go=0;
          break;
      }
    }

    /*
     * ----------------
     *  Timing
     * ----------------
     */
    double timerf; { // Change scope for 'old' var
      Uint32 old=timer;
      timer = SDL_GetTicks();
      timerf = (double)(timer-old)/1000.0; //0.0166... = 60FPS (1/timerf);
    }

    /*
     * ----------------
     *  Draw to buffer
     * ----------------
     */
    
    /*
     * --------------------------------
     *  Output to Window
     * --------------------------------
     */
     SDL_GL_SwapWindow(window1);
  }
 
  /*
   * --------------------------------
   *  Clean up after ourselves
   * --------------------------------
   */
   SDL_GL_DeleteContext(context1);
   SDL_DestroyWindow(window1);
   SDL_Quit();

  return 0;
}
