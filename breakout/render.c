#ifdef _WIN32
#include <Windows.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "include/structs.h"
#include "include/paddleAndBall.h"

void drawText(const char *text, TTF_Font *font, SDL_Color txtColour, coord loc) {
  SDL_Surface *text_surface = TTF_RenderUTF8_Blended(font, text, txtColour);
  if (text_surface == NULL) {
    fprintf(stderr, "Error generating text");
  }
  else {
    GLuint testTxt;
    surfaceToTexture(&testTxt, text_surface);

    glBindTexture(GL_TEXTURE_2D, testTxt);

    GLint matrixmode = 0;
    glGetIntegerv(GL_MATRIX_MODE, &matrixmode);
    glPushMatrix();
    glTranslated(loc.x - (text_surface->w / 2), loc.y, 0.0);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex3d(0.0, 0.0, 0.0);
    glTexCoord2f(0, 1); glVertex3d(0.0, -1 * text_surface->h, 0.0);
    glTexCoord2f(1, 1); glVertex3d(text_surface->w, -1 * text_surface->h, 0.0);
    glTexCoord2f(1, 0); glVertex3d(text_surface->w, 0.0, 0.0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, 0);

    glPopMatrix();
    glMatrixMode(matrixmode);
  }
}

void drawHearts(GLuint heartFullTex, GLuint heartEmptyTex, int playerLives, int winWidth, int winHeight) {
  coord locations[3] = { { .x = -19,.y = -32 },{ .x = 0,.y = -32 },{ .x = 19,.y = -32 } };

  for (int i = 0; i < 3; i++) {
    if (i > playerLives - 1) {
      glBindTexture(GL_TEXTURE_2D, heartEmptyTex);
    }
    else {
      glBindTexture(GL_TEXTURE_2D, heartFullTex);
    }
    GLint matrixmode = 0;
    glGetIntegerv(GL_MATRIX_MODE, &matrixmode);
    glPushMatrix();
    glTranslated(0.0, (winHeight / 2), 0.0);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex3d(0.0 + locations[i].x, 0.0 + locations[i].y, 0.0);
    glTexCoord2f(0, 1); glVertex3d(0.0 + locations[i].x, -15.0 + locations[i].y, 0.0);
    glTexCoord2f(1, 1); glVertex3d(15.0 + locations[i].x, -15.0 + locations[i].y, 0.0);
    glTexCoord2f(1, 0); glVertex3d(15.0 + locations[i].x, 0.0 + locations[i].y, 0.0);
    glEnd();

    glPopMatrix();
    glMatrixMode(matrixmode);
  }

  glBindTexture(GL_TEXTURE_2D, 0);
}

void drawBg(int winWidth, int winHeight, colour *c1, colour *c2) {
  GLint matrixmode = 0;
  glGetIntegerv(GL_MATRIX_MODE, &matrixmode);
  glPushMatrix();
  glTranslated((GLdouble)(-1 * winWidth / 2), (GLdouble)(-1 * winHeight / 2), 0.0);

  glBegin(GL_QUADS);
  //Top colour
  glColor3f(c1->r, c1->g, c1->b);
  glVertex2f(winWidth, 0.0);
  glVertex2f(0.0, 0.0);
  //Bottom colour
  glColor3f(c2->r, c2->g, c2->b);
  glVertex2f(0.0, winHeight);
  glVertex2f(winWidth, winHeight);
  glEnd();
}

void drawPaddle(paddle *thePaddle, int winWidth, int winHeight, colour *c) {
  GLint matrixmode = 0;
  glGetIntegerv(GL_MATRIX_MODE, &matrixmode);
  glPushMatrix();
  glTranslated((GLdouble)(thePaddle->x) - winWidth / 2, (GLdouble)(20 - winHeight / 2), 0.0);

  glBegin(GL_QUADS);
  glColor3f(c->r, c->g, c->b);
  glVertex3d(0.0, 0.0, 0.0);
  glVertex3d(0.0, (double)-1 * thePaddle->height, 0.0);
  glVertex3d((double)thePaddle->width, (double)-1 * thePaddle->height, 0.0);
  glVertex3d((double)thePaddle->width, 0.0, 0.0);
  glEnd();

  glPopMatrix();
  glMatrixMode(matrixmode);
}

void drawBall(ball *theBall, colour *c) {
  GLint matrixmode = 0;
  glGetIntegerv(GL_MATRIX_MODE, &matrixmode);
  glPushMatrix();
  glTranslated(theBall->x, theBall->y, 0.0);

  glBegin(GL_QUADS);
  glColor3f(c->r, c->g, c->b);
  glVertex3d(-1.0*theBall->radius, 1.0*theBall->radius, 0.0);
  glVertex3d(-1.0*theBall->radius, -1.0*theBall->radius, 0.0);
  glVertex3d(1.0*theBall->radius, -1.0*theBall->radius, 0.0);
  glVertex3d(1.0*theBall->radius, 1.0*theBall->radius, 0.0);
  glEnd();

  glPopMatrix();
  glMatrixMode(matrixmode);
}

void drawBricks(brick *bricks, int brickCount, int winWidth, int winHeight, colour *colours) {
  for (int i = 0; i < brickCount; i++) {
    if (bricks[i].destroyed == 0) {
      GLint matrixmode = 0;
      glGetIntegerv(GL_MATRIX_MODE, &matrixmode);
      glPushMatrix();
      glTranslated((double)bricks[i].x - (winWidth / 2), (double)bricks[i].y, 0.0);
      glBegin(GL_QUADS);
      glColor3f(
        colours[bricks[i].brickType - 1].r,
        colours[bricks[i].brickType - 1].g,
        colours[bricks[i].brickType - 1].b
      );
      glVertex3d(0.0, 0.0, 0.0);
      glVertex3d(0.0, (double)bricks[i].height, 0.0);
      glVertex3d((double)bricks[i].width, (double)bricks[i].height, 0.0);
      glVertex3d((double)bricks[i].width, 0.0, 0.0);
      glEnd();
      glPopMatrix();
      glMatrixMode(matrixmode);
    }
  }
}