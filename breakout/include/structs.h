

typedef struct coord {
  double x;
  double y;
} coord;

typedef struct colour {
  double r;
  double g;
  double b;
  double a;
} colour;

typedef struct paddle {
  int width;
  int height;
  double x;
} paddle;

typedef struct ball {
  double x;
  double y;
  double vX;
  double vY;
  double initVX;
  double initVY;
  double radius;
  char isColliding;
  char stickToPaddle;
} ball;

typedef struct brick {
  char brickType;
  int x;
  int y;
  int width;
  int height;
  char destroyed;
} brick;

typedef struct brickCD {
  char isLeaf;
  int x;
  int y;
  int width;
  int height;
  struct brickCD *container;
  int brickID;
} brickCD;
