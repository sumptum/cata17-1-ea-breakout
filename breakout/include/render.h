void drawHearts(GLuint heartFullTex, GLuint heartEmptyTex, int playerLives, int winWidth, int winHeight);
void drawPaddle(paddle *thePaddle, int winWidth, int winHeight, colour *c);
void drawBall(ball *theBall, colour *c);
void drawBricks(brick *bricks, int brickCount, int winWidth, int winHeight, colour *colours);
void drawBg(int winWidth, int winHeight, colour *c1, colour *c2);
void drawText(const char *text, TTF_Font *font, SDL_Color txtColour, coord loc);