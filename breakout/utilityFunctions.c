#include <math.h>
#include "include/structs.h"

double heronsFormula(coord a, coord b, coord c) {
  double A = sqrt(pow((a.x - b.x), 2) + pow((a.y - b.y), 2));
  double B = sqrt(pow((b.x - c.x), 2) + pow((b.y - c.y), 2));
  double C = sqrt(pow((c.x - a.x), 2) + pow((c.y - a.y), 2));
  double s = (A + B + C) / 2;
  return sqrt(s*(s - A)*(s - B)*(s - C));
}

char vertexWithinQuad(coord point, coord *quad) {
  double quadArea = heronsFormula(quad[0], quad[1], quad[2]) + heronsFormula(quad[1], quad[2], quad[3]);
  double testArea = 0;
  testArea += heronsFormula(point, quad[0], quad[1]);
  testArea += heronsFormula(point, quad[1], quad[2]);
  testArea += heronsFormula(point, quad[2], quad[3]);
  testArea += heronsFormula(point, quad[3], quad[0]);
  if ((int)testArea == (int)quadArea) {
    return 1;
  }
  else {
    return 0;
  }
}

