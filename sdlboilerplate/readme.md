

# Boilerplate Code

Creating a bare-minimum C file for setting up SDL with a single window and renderer (result is **sdlboilerplate.c**)

## pointy.c

> Compile using `make pointy`

Original file provided by Eike Anderson as part of an exercise to introduce SDL

## pointy_modified.c

> Compile using `make pointymod`

Completed pointy.c file (creating a 'line' from rendered points that resizes with the window, as well as a circle positioned centrally)

## sdlboilerplate.c 

> Compile using `make boilerplate`

A template file created using the completed **pointy.c** file as a base for abstraction

