
#include <SDL2/SDL.h>
#include <stdio.h>

int main(void) {

  /*
   * RETURN CODES
   *  0: Success
   *  1: SDL init fail
   *  2: Error creating Window
   *  3: Error creating renderer
   *
   */
  
  /* ----------------
   *  SDL Init
   * ----------------
   */
  if (SDL_Init(SDL_INIT_EVERYTHING)!=0) {
    perror("[FAILED] Initialising SDL: ");
    return 1;
  }

  int go; //Var for loop control
  
  /* ----------------
   *  Main Window
   * ----------------
   * Create the main output Window for SDL
   */

  // Window vars
  int winPosX = 100; int winPosY = 100;
  int winWidth = 640; int winHeight = 480;

  SDL_Window *window1 = SDL_CreateWindow(
    "Main Window",
    winPosX, winPosY,
    winWidth, winHeight,
    SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
  );
  if (window1==NULL) {
    fprintf(stderr, "[FAILED] Creating Window window1");
    return 2; //Error creating Window
  }

  /*
   * ----------------
   *  Renderer
   * ----------------
   * Creates the main renderer (tied to main window)
   */

  SDL_Renderer * renderer1 = SDL_CreateRenderer(window1, -1, 0);
  if (renderer1==NULL) {
    fprintf(stderr, "[FAILED] Creating renderer renderer1 for window1");
    return 3;
  }

  go=1;
  while(go) {

    /*
     * ----------------
     *  Event handling
     * ----------------
     */
    SDL_Event incomingEvent;
    while (SDL_PollEvent(&incomingEvent)) {
      switch (incomingEvent.type) {
        case SDL_QUIT:
          // User requested program quit - e.g. window close
          go=0;
          break;
      }
    }

    /*
     * ----------------
     *  Draw to buffer
     * ----------------
     */
    SDL_SetRenderDrawColor(renderer1, 0xFF, 0x00, 0x0, 0xFF); //RGBA
    SDL_RenderClear(renderer1);
    
    /*
     * --------------------------------
     *  Output to Window
     * --------------------------------
     */
     SDL_RenderPresent(renderer1);
     SDL_Delay(10);
  }
 
  /*
   * --------------------------------
   *  Clean up after ourselves
   * --------------------------------
   */
   SDL_DestroyWindow(window1);
   SDL_Quit();

  return 0;
}
