
// This is the main SDL include file
#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void pointy(SDL_Renderer *renderer, int width, int height)
{
	int x=1,y=1;
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	// for(x=1,y=1;x<width-1;x+=((width+100)/100),y+=((height+100)/100))
	// {
	// 	SDL_RenderDrawPoint(renderer,x,y);
	// }
	float ratio=(float)height/(float)width;
        //printf("width: %i, height: %i, ratio: %f\n",width,height,ratio);
        int loop=0;
        while(x<width-1 && y<height-1) {
          loop++;
          x+=1; y=(int)(ratio*(float)x);
          SDL_RenderDrawPoint(renderer,x,y);
        } loop=0; x=0; y=0;
        while(x<width-1 && y<height-1) {
          loop++;
          y+=1; x=(int)((float)y/ratio);
          SDL_RenderDrawPoint(renderer,x,y);
        } loop=0; x=0; y=0;

        /* CIRCLE */

        SDL_SetRenderDrawColor(renderer, 0, 255, 64, 255);

        // x=width/2; y=height/2;
        // float pi=3.141;
        // int r=64;

        // x=0;y=0;
        // 
        // for(int i=0,j=0;i<4;i++) {
        //   if(i%2=0){j=1;} if(i%2=1){j=-1}; //Every other loop
        //   if(i<2)  {k=1;} else     {k=-1}; //Last two loops
        //   while (x<width-1 && y<height-1) {
        //     loop++;
        //     x+=1; y=sqrt(pow(r,2)-pow(x,2));
        //     SDL_RenderDrawPoint(renderer,x+width/2,y+height/2);
        //   }
        // }

        // y=plusminusSqrt(r**2-x**2)

        //printf("[%i] [x,y]: [%i, %i], ratio %f\n",loop,x,y,ratio);
        

        int radius=64;
        int x0=width/2;
        int y0=height/2;
        x = radius-1;
        y = 0;
        int dx = 1;
        int dy = 1;
        int err = dx - (radius << 1);

        while (x >= y)
        {
            SDL_RenderDrawPoint(renderer,x0 + x, y0 + y);
            SDL_RenderDrawPoint(renderer,x0 + y, y0 + x);
            SDL_RenderDrawPoint(renderer,x0 - y, y0 + x);
            SDL_RenderDrawPoint(renderer,x0 - x, y0 + y);
            SDL_RenderDrawPoint(renderer,x0 - x, y0 - y);
            SDL_RenderDrawPoint(renderer,x0 - y, y0 - x);
            SDL_RenderDrawPoint(renderer,x0 + y, y0 - x);
            SDL_RenderDrawPoint(renderer,x0 + x, y0 - y);

            if (err <= 0)
            {
                y++;
                err += dy;
                dy += 2;
            }
            if (err > 0)
            {
                x--;
                dx += 2;
                err += dx - (radius << 1);
            }
        }

}

int main(void)
{
	/* These are some variables to help show you what the parameters are for the initialisation function.
	   You can experiment with the numbers to see what they do. */
	int winPosX = 100;
	int winPosY = 100;
	int winWidth = 640;
	int winHeight = 480;
	int go;

	/* This is our initialisation phase

	   SDL_Init is the main initialisation function for SDL
	   It takes a 'flag' parameter which we use to tell SDL what systems we are going to use
	   Here, we want to initialise everything, so we give it the flag for this.
	   This function also returns an error value if something goes wrong,
	   so we can put this straight in an 'if' statement to check and exit if need be */
	if( SDL_Init( SDL_INIT_EVERYTHING ) != 0 )
	{
		/* Something went very wrong in the initialisation, all we can do is exit */
		perror("Whoops! Something went very wrong, cannot initialise SDL :(");
		return -1;
	}

	/* Now we have got SDL initialised, we are ready to create a window! */
	SDL_Window *window = SDL_CreateWindow("My Pointy Window!!!",  /* The first parameter is the window title */
		winPosX, winPosY,
		winWidth, winHeight,
		SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	/* The last parameter lets us specify a number of options.
	   Here, we tell SDL that we want the window to be shown and that it can be resized.
	   You can learn more about SDL_CreateWindow here: https://wiki.libsdl.org/SDL_CreateWindow?highlight=%28\bCategoryVideo\b%29|%28CategoryEnum%29|%28CategoryStruct%29
	   The flags you can pass in for the last parameter are listed here: https://wiki.libsdl.org/SDL_WindowFlags

	   The SDL_CreateWindow function returns an SDL_Window.
	   This is a structure which contains all the data about our window (size, position, etc).
	   We will also need this when we want to draw things to the window.
	   This is therefore quite important we do not lose it! */ 

	/* The SDL_Renderer is a structure that handles rendering.
	   It will store all of SDL's internal rendering related settings.
	   When we create it we tell it which SDL_Window we want it to render to.
	   That renderer can only be used for this window (yes, this means we can have multiple windows). */
	SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);


	/* We are now preparing for our main loop.
	   This loop will keep going round until we exit from our program by changing the int 'go' to the value false (0).
	   This loop is an important concept and forms the basis of most SDL programs you will be writing.
	   Within this loop we generally do the following things:
	     * Check for input from the user (and do something about it!)
	     * Update our graphics
	     * Draw our graphics
    */
	go = 1;
	while( go )
	{
		/* Here we are going to check for any input events.
		   Basically when you press the keyboard or move the mouse, the parameters are stored as something called an 'event' or 'message'.
		   SDL has a queue of events. We need to check for each event and then do something about it (called 'event handling').
		   The SDL_Event is the data type for the event. */
		SDL_Event incomingEvent;


		/* SDL_PollEvent will check if there is an event in the queue - this is the program's 'message pump'.
		   If there is nothing in the queue it will not sit and wait around for an event to come along (there are functions which do this,
		   and that can be useful too!). Instead for an empty queue it will simply return 'false' (0).
		   If there is an event, the function will return 'true' (!=0) and it will fill the 'incomingEvent' we have given it as a parameter with the event data */
		while( SDL_PollEvent( &incomingEvent ) )
		{
			/* If we get in here, we have an event and need to figure out what to do with it.
			   For now, we will just use a switch based on the event's type */
			switch( incomingEvent.type )
			{
			case SDL_QUIT:
				/* The event type is SDL_QUIT.
				   This means we have been asked to quit - probably the user clicked on the 'x' at the top right corner of the window.
				   To quit we need to set our 'go' variable to false (0) so that we can escape out of the main loop. */
				go = 0;
				break;

				/* If you want to learn more about event handling and different SDL event types, see:
				  https://wiki.libsdl.org/SDL_Event
				  and also: https://wiki.libsdl.org/SDL_EventType */
			}
		}



		/* Draw our graphics */

		/* Start by clearing what was drawn before */
		/* Set the colour for drawing */
		SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0xFF, 0xFF );
		/* Clear the entire screen to our selected colour */
		SDL_RenderClear( renderer );

                /* Find current window size */
                SDL_GetWindowSize( window, &winWidth, &winHeight);

		/* Call our pointy drawing */
		pointy( renderer, winWidth, winHeight );


		/* This tells the renderer to actually show its contents to the screen. */
		SDL_RenderPresent(renderer);

		/* Turns out SDL_WaitEvent just does an SDL_PollEvent and then an SDL_Delay(10) if there is nothing to do! */
		SDL_Delay(10);

                //go=0;
	}

	/* If we get outside the main loop, it means our user has requested we exit. */


	/* Our cleanup phase, hopefully fairly self-explanatory ;) */
	SDL_DestroyWindow( window );
	SDL_Quit();

	return 0;
}
