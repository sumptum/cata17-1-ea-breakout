Breakout
========

CATA 2017 Programming Principles project: Breakout game

Getting Started
---------------

To download and run, follow the instructions below.

### Prerequisites

**OpenGL** compatible graphics driver

#### Clang

*Default compiler used for this project*  
See the [Clang getting started page](https://clang.llvm.org/get_started.html) for info on compiling.
Pre-build packages available on Debian/Ubuntu:

> \# apt-get install clang

and CentOS/Fedora:

> \# yum install clang

#### SDL
*Simple DirectMedia Layer library*  
See the [SDL wiki page](https://wiki.libsdl.org/Installation) for installation instructions

### Installing

Clone the repository (into a new directory *breakout_dir*)

> $ git clone https://bitbucket.org/sumptum/cata17-1-ef-breakout.git breakout_dir

#### Breakout

Change to the breakout directory inside the repository (contains the main program) and run `make` to compile

> $ cd breakout_dir/breakout  
> $ make

To run, execute the output binary - **breakout** (takes no args)

> $ ./breakout

##### Troubleshooting

**Permission denied:** Usually this is a result of the binary not having the correct permission bits set.  
The user must have permission to execute the binary, e.g.

> $ chmod +x ./breakout

Exit codes:

Code | Meaning
---- | -------
0    | Completed successfully
1    | SDL initialisation failed
2    | Error creating window
3    | Error creating renderer

Stderr may contain more information. See source for more

#### (Others)

Other folders contain test and reference code, detailed below.  
For build instructions, view the readme files provided within

Running `make` with no arguments will prompt, e.g.:  

> $ cd breakout_dir/sdlboilerplate  
> $ make  
>   .... OUTPUT TRUNCATED ....  
>   compile using `make pointy`  
>   .... OUTPUT TRUNCATED ....  

### Contents

#### breakout

This is the main folder for the project, containing the code for the breakout game

#### (Others)

Additional subfolders contain reference, experimental or tangential code used in creating neccesary components of the program.  
For credits and usage details, see the readme files provided within

### Authors

* **Joe Adams** - *Owner* - [s5067322@bournemouth.ac.uk](mailto:s5067322@bournemouth.ac.uk) / [dev@joea.co.uk](mailto:dev@joea.co.uk)
